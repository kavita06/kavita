import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from './shared/shared.module';


import { MaterialModule } from './shared/material/material.module';
import { MatExpansionModule} from '@angular/material/expansion';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule, FormsModule,FormBuilder } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import {Ng2TelInputModule} from 'ng2-tel-input';

import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';










@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,Ng2TelInputModule,MatInputModule,
    AppRoutingModule,MatCardModule,MatMenuModule,MatDatepickerModule,
    BrowserAnimationsModule,MatCardModule,ReactiveFormsModule,FormsModule,MaterialModule,MatExpansionModule,MatFormFieldModule,
    HttpClientModule,SharedModule,ToastrModule,
    ToastrModule.forRoot({
      positionClass:'toast-top-full-width'
    }),
  ],
  providers:[NgxSpinnerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
