import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ListComponent } from './list/list.component';
import { MoreuserComponent } from './moreuser/moreuser.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [{
  path: '', component: HomeComponent
},{
  path:'list',component: ListComponent
},{
  path:'user',component:UserComponent
},{
  path:'moreuser',component:MoreuserComponent
}, 
{
  path: 'edit/:id', component: UserComponent

}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
