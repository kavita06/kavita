import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/shared/material/material.module';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {Ng2TelInputModule} from 'ng2-tel-input';







import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { ListComponent } from './list/list.component';
import { MoreuserComponent } from './moreuser/moreuser.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule,FormBuilder } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import {MatFormFieldModule} from '@angular/material/form-field';




@NgModule({
  declarations: [
    HomeComponent,
    UserComponent,
    ListComponent,
    MoreuserComponent
  ],
  imports: [
    CommonModule,MatDatepickerModule,MatInputModule,Ng2TelInputModule,
    PagesRoutingModule,MatCardModule,MatButtonModule,FormsModule,ReactiveFormsModule,MatMenuModule,MaterialModule,MatFormFieldModule,
  ]
})
export class PagesModule { }
