import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, } from '@angular/forms';
import { Params, Router } from '@angular/router';
import { UserService } from 'src/app/user.service';
import { MatFormFieldModule } from '@angular/material/form-field';
const today = new Date();
const month = today.getMonth();
const year = today.getFullYear()


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  submitted: boolean = false;

  profileform: FormGroup

  router: any;
  compaignOne: any;


  user() {
    console.warn(this.profileform.value);
    this.service.post('merchant', this.profileform.value).subscribe((record: any) => {
      this.route.navigateByUrl('/')
      console.log(record);
    });
  }
  constructor(private service: UserService, private route: Router) {
    this.profileform = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', [Validators.required]),

      dob: new FormControl('', [Validators.required]),

      countrycode: new FormControl('', [Validators.required]),
      address: new FormControl('', Validators.required)
    })

  }
  onupdate() { }


  ngOnInit(): void {
  }

}
