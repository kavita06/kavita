import { Component, OnInit,ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/user.service';

export interface Element {
  name: number;
  email: number;
  phone: number;
  dob: string;
  countryCode:number;
  address:number;
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

displayedColumns = ['name', 'email', 'phone', 'dob', 'countryCode','address','action'];
  dataSource = new MatTableDataSource<Element>();
  
  UserService: any;
  id:any;
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    console.log(filterValue)
      
    }
constructor(private userservice:UserService, private route: Router, private http: HttpClient, private router: ActivatedRoute) {
  this.dataSource=new MatTableDataSource

 }
 data(){
  this.userservice.get('merchant').subscribe((record:any)=>{
    console.log(record)
    this.dataSource=record.data
  })
 }
 
 

  ngOnInit(): void {this.data();}

  
  delete(id:string){
    if(confirm('Are you sure to delete record?'))
    this.userservice.delete('merchant/'+id).subscribe((res:any)=>{
      alert('record deleted successfully')
      console.log(res);
    })
  }

  editid(id:string){
    this.userservice.getby('merchant/'+id).subscribe((res:any)=>{
      console.log(res);
      this.route.navigate(['user/',res]);

      
    })

  }
  


}
