import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoreuserComponent } from './moreuser.component';

describe('MoreuserComponent', () => {
  let component: MoreuserComponent;
  let fixture: ComponentFixture<MoreuserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoreuserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoreuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
