import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseurl= environment.base_url


  constructor(private http:HttpClient) { }
  get(url:any){
    return this.http.get(this.baseurl+url);
  }
  post(url:any,body:any){
    return this.http.post(this.baseurl+url,body);
  }
  delete(url:string){
    return this.http.delete(this.baseurl+url)
  }
  getby(url:any){
    return this.http.get(this.baseurl+url)
  }
  put(url:any,body:any){
    return this.http.put(this.baseurl+url,body);
  }
}

